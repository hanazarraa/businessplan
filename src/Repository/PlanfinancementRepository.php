<?php

namespace App\Repository;

use App\Entity\Planfinancement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Planfinancement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Planfinancement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Planfinancement[]    findAll()
 * @method Planfinancement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanfinancementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Planfinancement::class);
    }

    // /**
    //  * @return Planfinancement[] Returns an array of Planfinancement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Planfinancement
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
