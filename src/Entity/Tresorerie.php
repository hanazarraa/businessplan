<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TresorerieRepository")
 */
class Tresorerie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Businessplan", inversedBy="tresoreries")
     */
    private $businessplan;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $capital = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $CompteCourant = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $Subventions = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $Avances = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $Autre = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $Autredecaiseement = [];

    /**
     * @ORM\Column(type="integer")
     */
    private $year;

    public function getBusinessplan(): ?Businessplan
    {
        return $this->businessplan;
    }

    public function setBusinessplan(?Businessplan $businessplan): self
    {
        $this->businessplan = $businessplan;

        return $this;
    }

    public function getCapital(): ?array
    {
        return $this->capital;
    }

    public function setCapital(?array $capital): self
    {
        $this->capital = $capital;

        return $this;
    }

    public function getCompteCourant(): ?array
    {
        return $this->CompteCourant;
    }

    public function setCompteCourant(?array $CompteCourant): self
    {
        $this->CompteCourant = $CompteCourant;

        return $this;
    }

    public function getSubventions(): ?array
    {
        return $this->Subventions;
    }

    public function setSubventions(?array $Subventions): self
    {
        $this->Subventions = $Subventions;

        return $this;
    }

    public function getAvances(): ?array
    {
        return $this->Avances;
    }

    public function setAvances(?array $Avances): self
    {
        $this->Avances = $Avances;

        return $this;
    }

    public function getAutre(): ?array
    {
        return $this->Autre;
    }

    public function setAutre(?array $Autre): self
    {
        $this->Autre = $Autre;

        return $this;
    }

    public function getAutredecaiseement(): ?array
    {
        return $this->Autredecaiseement;
    }

    public function setAutredecaiseement(?array $Autredecaiseement): self
    {
        $this->Autredecaiseement = $Autredecaiseement;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }



}
