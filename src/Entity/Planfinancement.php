<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlanfinancementRepository")
 */
class Planfinancement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $autre = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $totalcapital = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $totalcompte = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $totalsubventions = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $totalavance = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\businessplan", inversedBy="planfinancements")
     */
    private $businessplan;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAutre(): ?array
    {
        return $this->autre;
    }

    public function setAutre(?array $autre): self
    {
        $this->autre = $autre;

        return $this;
    }

    public function getTotalcapital(): ?array
    {
        return $this->totalcapital;
    }

    public function setTotalcapital(?array $totalcapital): self
    {
        $this->totalcapital = $totalcapital;

        return $this;
    }

    public function getTotalcompte(): ?array
    {
        return $this->totalcompte;
    }

    public function setTotalcompte(?array $totalcompte): self
    {
        $this->totalcompte = $totalcompte;

        return $this;
    }

    public function getTotalsubventions(): ?array
    {
        return $this->totalsubventions;
    }

    public function setTotalsubventions(?array $totalsubventions): self
    {
        $this->totalsubventions = $totalsubventions;

        return $this;
    }

    public function getTotalavance(): ?array
    {
        return $this->totalavance;
    }

    public function setTotalavance(?array $totalavance): self
    {
        $this->totalavance = $totalavance;

        return $this;
    }

    public function getBusinessplan(): ?businessplan
    {
        return $this->businessplan;
    }

    public function setBusinessplan(?businessplan $businessplan): self
    {
        $this->businessplan = $businessplan;

        return $this;
    }
}
