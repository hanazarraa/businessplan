<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BFRRepository")
 */
class BFR
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $stocks = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $Taxliabilites = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $totalliabilites = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $totaldebts = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Businessplan", inversedBy="bFRs")
     */
    private $businessplan;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStocks(): ?array
    {
        return $this->stocks;
    }

    public function setStocks(?array $stocks): self
    {
        $this->stocks = $stocks;

        return $this;
    }

    public function getTaxliabilites(): ?array
    {
        return $this->Taxliabilites;
    }

    public function setTaxliabilites(?array $Taxliabilites): self
    {
        $this->Taxliabilites = $Taxliabilites;

        return $this;
    }

    public function getTotalliabilites(): ?array
    {
        return $this->totalliabilites;
    }

    public function setTotalliabilites(?array $totalliabilites): self
    {
        $this->totalliabilites = $totalliabilites;

        return $this;
    }

    public function getTotaldebts(): ?array
    {
        return $this->totaldebts;
    }

    public function setTotaldebts(?array $totaldebts): self
    {
        $this->totaldebts = $totaldebts;

        return $this;
    }

    public function getBusinessplan(): ?Businessplan
    {
        return $this->businessplan;
    }

    public function setBusinessplan(?Businessplan $businessplan): self
    {
        $this->businessplan = $businessplan;

        return $this;
    }
}
