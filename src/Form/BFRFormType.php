<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
class BFRFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->
        
        add('stocks',CollectionType::class,[
            'label' => false,
            'entry_type' => NumberType::class,
            'required'=>false,
           
           
        ])
        ->add('Taxliabilites',CollectionType::class,[
            'label' => false,
            'entry_type' => NumberType::class,
            'required'=>false,
           
        ])
        ->add('totalliabilites',CollectionType::class,[
            'label' => false,
            'entry_type' => NumberType::class,
            'required'=>false,
           
           
        ])
        ->add('totaldebts',CollectionType::class,[
            'label' => false,
            'entry_type' => NumberType::class,
            'required'=>false,
           
           
        ])
        ->add('Valider', SubmitType::class);
    }
}