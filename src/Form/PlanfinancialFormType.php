<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;

class PlanfinancialFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder-> add('Autre',CollectionType::class ,[
            'entry_type' => NumberType::class
        ])
        ->add('totalcapital',CollectionType::class,[
            'entry_type' => NumberType::class
        ])
        ->add('totalcompte',CollectionType::class,[
            'entry_type' => NumberType::class
        ])
        ->add('totalsubventions',CollectionType::class,[
            'entry_type' => NumberType::class
        ])
        ->add('totalavance',CollectionType::class,[
            'entry_type' => NumberType::class
        ])
        ->add('Valider',SubmitType::class) ;

    }

}