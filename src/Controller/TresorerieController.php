<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\SalesRepository;
use App\Repository\ProductRepository;
use App\Repository\SalesdetailledRepository;
use App\Entity\Tresorerie;
use App\Entity\Loans;
use App\Entity\TVA;
use App\Entity\impotsursociete;
use App\Form\TreasuryFormType;

class TresorerieController extends AbstractController
{
    /**
     * @Route("/{_locale}/dashboard/my-business-plan/tresorerie-annee-{id}" , name="tresorerie")
     */
    public function index(Request $request ,ProductRepository $productRepository ,SalesRepository $SalesRepository,SalesdetailledRepository $SalesdetailledRepository,$id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $businessSession =$this->container->get('session')->get('business');
        $years = $businessSession->getNumberofyears();
        $tresorerie = $entityManager->getRepository(Tresorerie::class)->findBy(['businessplan' => $businessSession ]);
        $loans = $entityManager->getRepository(Loans::class)->findBy(['businessplan' => $businessSession ]);
        $TVA = $entityManager->getRepository(TVA::class)->findBy(['businessplan' => $businessSession ]);
        $IS = $entityManager->getRepository(impotsursociete::class)->findBy(['businessplan' => $businessSession ]);
        $exsist = True ;
        if($tresorerie == []){
        $capital = [];
        $compte_courant = [];
        $subventions = [] ;
        $avances = [] ;
        $autre = [] ;
        $autredecaissement = [];
        for($i = 0 ; $i<12 ; $i++){
            $capital[$i] = "0.00";
            $compte_courant[$i] = "0.00";
            $subventions[$i] = "0.00";
            $avances[$i] = "0.00";
            $autre[$i] = "0.00";
            $autredecaissement[$i] = "0.00";
        }
        for($x=0;$x<$years;$x++){
        $tresorerie = new Tresorerie();
        $tresorerie->setBusinessplan($businessSession);
        $tresorerie->setCapital($capital);
        $tresorerie->setCompteCourant($compte_courant);
        $tresorerie->setSubventions($subventions);
        $tresorerie->setAvances($avances);
        $tresorerie->setAutre($autre);
        $tresorerie->setAutredecaiseement($autredecaissement);
        $tresorerie->setYear($x);
        $entityManager->merge($tresorerie);
        $exsist = false ;
        $entityManager->flush();}
        }
        if($exsist == false){
            $form = $this->createForm(TreasuryFormType::class , $tresorerie);
        }
        else{
        $form = $this->createForm(TreasuryFormType::class , $tresorerie[$id]);}
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $tresorerie = $form->getData();
            
              
              $entityManager->merge($tresorerie);
              $entityManager->flush();
             
        }
        //----------------------sales&Purchase data ---------------------------------------//
        $response = $this->forward('App\Controller\SalesController::receiptyears', [
            'request'  => $request,
            'SalesdetailledRepository' => $SalesdetailledRepository,
            'id' => $id ,
            'productRepository' => $productRepository,
            'SalesRepository' => $SalesRepository,
        ]); 

        $response = $this->forward('App\Controller\PurchaseController::disbursement', [
            'SalesdetailledRepository' => $SalesdetailledRepository,
            'productRepository' => $productRepository,
            'id' => $id ,
            'Request'  => $request,
            'SalesRepository' => $SalesRepository,
        ]); 
        $response = $this->forward('App\Controller\GeneralexpensesController::detail', [
            'id' => $id ,
            'Request'  => $request,
            
        ]); 
        $response = $this->forward('App\Controller\StaffController::detail', [
            'Request'  => $request,
            'id' => $id ,
            'ProductRepository' => $productRepository,
            'SalesRepository' => $SalesRepository,
        ]); 
        $response = $this->forward('App\Controller\InvestmentsController::detail', [
            'Request'  => $request,
            'id' => $id ,
        ]);
        $response = $this->forward('App\Controller\TVAController::index', [
            'id' => $id ,
            'Request'  => $request,
            'ProductRepository' => $productRepository,
            'SalesdetailledRepository' => $SalesdetailledRepository,
            'SalesRepository' => $SalesRepository,
        ]);
        $receipt = SalesController::getReceipt();
        $Disbursementtotaldecaisse =  PurchaseController::gettotaldecaisser();
        $AdministrationTTC = GeneralexpensesController::getAdministrationTTC();
        $ProductionTTC = GeneralexpensesController::getProductionTTC();
        $CommercialTTC = GeneralexpensesController::getCommercialTTC();
        $ReseachTTC = GeneralexpensesController::getReasearchTTC();
        $Netapayer = StaffController::getNetapayer();
        $NetapayerPro = StaffController::getNetapayerPro();
        $NetapayerCom = StaffController::getNetapayerCom();
        $NetapayerRec = StaffController::getNetapayerRec();
        $StaffchargeAdm  = StaffController::getDecaissementchargesAdm();
        $StaffchargePro  = StaffController::getDecaissementchargesPro();
        $StaffchargeCom  = StaffController::getDecaissementchargesCom();
        $StaffchargeRec  = StaffController::getDecaissementchargesRec();
        $ImmobilisationsAdm = InvestmentsController::getInvestTTCAdm();
        $ImmobilisationsPro = InvestmentsController::getInvestTTCPro();
        $ImmobilisationsCom = InvestmentsController::getInvestTTCCom();
        $ImmobilisationsRec = InvestmentsController::getInvestTTCRec();
        $DecaissementTVA = TVAController::gettvaremboursement();
      
       //------------------------end ----------------------------------------------//
       //-------------------------Loans data----------------------------------------//
       for($i=0;$i<$years;$i++){
       for($x=0;$x<12;$x++){    
       $Emprunt[$i][$x] = "0.00";
       $RemboursementEmprunt[$i][$x] = "0.00";

       }}
       for($x = 0 ; $x < $years * 12 ; $x++){
        $RemboursementEmpruntAllMonth[$x] = "0.00";
       }
       if($ImmobilisationsAdm == null){
        for($x=0;$x<12;$x++){
            $ImmobilisationsAdm[$x] = "0.00";
        }
       }
       if($ImmobilisationsPro == null){
        for($x=0;$x<12;$x++){
        $ImmobilisationsPro[$x] = "0.00";
       }}
       if($ImmobilisationsCom == null){
        for($x=0;$x<12;$x++){
        $ImmobilisationsCom[$x] = "0.00";
       }}
       if($ImmobilisationsRec == null){
        for($x=0;$x<12;$x++){
        $ImmobilisationsRec[$x] = "0.00";
       }}
       
       
       foreach($loans as $key=>$value){
        $amount = $value->getAmount(); 
        $taux = $value->getTaux();
        $firstpaymentdate = $value->getFirstpaymentdate();
        $dateLoans = $value->getLoandate();
        $diff = $firstpaymentdate->diff($dateLoans);
        
        $duration = $value->getDuration();
        $Montantecheance = (($amount * (($taux /100)/12))  / (1 - ((1+ ($taux / 100 / 12))** (-$duration *12 + $diff->m))));
        $Montantecheancearrondi = round($Montantecheance, 2) ;
        
        $startyearLoans = $dateLoans->format('Y');
        $startMonthLoans = $dateLoans->format('m'); 
        $startfirstpaymentmounth = $firstpaymentdate->format('m') ; 
        //dump($startMonthLoans - 1);die();
        if($startyearLoans >= $businessSession->getStartyear()){
            $diffirenceYearsLoansandBP =  $startyearLoans - $businessSession->getStartyear();
        }
        else{
            $diffirenceYearsLoansandBP = 0 ;
        }
        $Emprunt[$diffirenceYearsLoansandBP][$startMonthLoans - 1 ] += $amount ;
      
        for($i=$diffirenceYearsLoansandBP * 12 + ($startfirstpaymentmounth - 1);$i<($duration *12 )+ ($startMonthLoans - 1);$i++){
            
            $RemboursementEmpruntAllMonth[$i] +=   $Montantecheancearrondi;
        }
       
       }
       //--------------------------preparer la liste final Remboursement------------------------------//
        $y  = 0 ;
        $M = 0 ;
        for($x=0;$x<$years * 12;$x++){
        $FinalRemboursement[$y][$M] = $RemboursementEmpruntAllMonth[$x];
        $M++;
        if($M==12){
        $y++;
        $M = 0;
        }
        }
  
       
       //-------------------------end Loans-----------------------------------------------------------//
       //------------------------------TVARemboursement---------------------------------------//
       if($TVA != []){
       $RemboursemetTVA = $TVA[$id]->getRemboursement();}
       
       //------------------------------endTVA------------------------------------------------//
       //----------------------------Remboursement de credit d'impot--------------------------//
        if($IS != []){
        $RemboursementCIR = $IS[$id]->getRemboursement();
       //----------------------------end Remboursement----------------------------------------//
       //-----------------------------Impôt sur les Sociétés----------------------------------//
       $Impotsursociete = $IS[$id]->getVersement();}
       
       if($TVA == []){
        for($x = 0 ; $x<12 ; $x++){
            $RemboursemetTVA[$x] = "0.00";
        }}
        if($IS == []){
            for($x = 0 ; $x<12 ; $x++){
                $RemboursementCIR[$x] = "0.00";
                $Impotsursociete[$x] = "0.00";
        }}
       //-----------------------------end IS--------------------------------------------------//
       //----------------------------Tresory inputs-------------------------------------------//
       $inputs = $entityManager->getRepository(Tresorerie::class)->findBy(['businessplan' => $businessSession ]);
       $newCapital = $inputs[$id]->getCapital();
       $newComptecourant = $inputs[$id]->getCompteCourant();
       $newSubventions = $inputs[$id]->getSubventions();
       $newAvances = $inputs[$id]->getAvances();
       $newAutre = $inputs[$id]->getAutre();
       $newAutredecaissement = $inputs[$id]->getAutredecaiseement();
       //----------------------------End Tresory----------------------------------------------//
        
        return $this->render('tresorerie/index.html.twig', [
            'business' => $businessSession,'ventesTTC' => $receipt[$id] ,'form' => $form->createView() ,'Emprunt' => $Emprunt[$id],
            'RemboursmentTVA'=> $RemboursemetTVA , 'RemboursmentCIR' =>  $RemboursementCIR, 'capital' => $newCapital , 'Comptecourant'=> $newComptecourant ,
            'Subventions' => $newSubventions , 'Avances' => $newAvances , 'AutreEncaissement' =>  $newAutre  ,'newAutredecaissement' => $newAutredecaissement ,'AchatTTC' => $Disbursementtotaldecaisse[$id] ,
            'AdministrationTTC' =>  $AdministrationTTC , 'ProductionTTC' => $ProductionTTC , 'CommercialTTC' => $CommercialTTC , 'ReseachTTC' => $ReseachTTC ,
            'Netapayer' => $Netapayer[$id] , 'NetapayerPro' => $NetapayerPro[$id] , 'NetapayerCom' => $NetapayerCom[$id]  , 'NetapayerRec'=>$NetapayerRec[$id],
            'StaffchargeAdm' => $StaffchargeAdm[$id] , 'StaffchargePro' => $StaffchargePro[$id] , 'StaffchargeCom' => $StaffchargeCom[$id] , 'StaffchargeRec' => $StaffchargeRec[$id],
            'ImmobilisationsAdm' => $ImmobilisationsAdm , 'ImmobilisationsPro' => $ImmobilisationsPro , 'ImmobilisationsCom' => $ImmobilisationsCom , 'ImmobilisationsRec' => $ImmobilisationsRec,
            'DecaissementTVA' => $DecaissementTVA[$id] , 'Impotsursociete' => $Impotsursociete , 'RemboursementEmprunt' => $FinalRemboursement[$id],
         ]);
    }
}
