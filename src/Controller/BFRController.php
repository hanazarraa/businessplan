<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\SalesdetailledRepository;
use App\Repository\ProductRepository;
use App\Repository\SalesRepository;
use App\Entity\BFR;
use App\Form\BFRFormType;
use Symfony\Component\HttpFoundation\Request;
/**
* @Route("/{_locale}/dashboard/my-business-plan/BFR")
 */
class BFRController extends AbstractController
{
    private $Compteclientforshow;
    private $tvacredit ;
    private $finaltvacredit;
    private $tvaadecaisser;
    private $finaltvadecaisser;
    private $creanceFiscales ;
    private $finalcreanceFiscales;
    private $variationFiscales;
    private $variationcreances;
    private $DettesFiscales ;
    private $finalDettesFiscales;
    private $DettesSociale;
    private $VariationDettes ;
    private $totalwithname ;
    private $variationBFR;
    private $totalCA;

    static $variationBFRstatic;
    static $variationglobalBFR ;
    /**
     * @Route("/", name="BFR")
     */
    public function index(Request $request,SalesdetailledRepository $SalesdetailledRepository,ProductRepository $productRepository,SalesRepository $SalesRepository)
    {
        $id = 1 ;
        $businessSession =$this->container->get('session')->get('business');
        $years = $businessSession->getNumberofyears();
        $rangeofdetail  = $businessSession->getRangeofdetail();
        $entityManager = $this->getDoctrine()->getManager();
        $rangeofglobal = $years  - $rangeofdetail ;
        $bfr = $entityManager->getRepository(BFR::class)->findBy(['businessplan' => $businessSession ]);
        $exsist = True ;
        $stock = [];
        $taxliabilities = [];
        $totaldebt = [];
        //initialiser les listes globale 
        for($i = 0 ; $i<$years + 1 ; $i++){
        $variationtotalliabilitesglobal[$i] = "0.00" ;
        }
        //initialiser les listes detailler
        for($i = 0 ; $i<$years + 1 ; $i++){
            $stock[$i] = "0.00" ;
            $taxliabilities[$i] = "0.00";
            $totaldebt[$i] = "0.00";
            }
        if($bfr == []){
            $stocks = [];
            $Taxliabilites = [];
            $totalliabilites = [];
            $totaldebts = [];
            //initialiser les listes a remplire de BFR
            for($i = 0 ; $i<$rangeofglobal ; $i++){
                
                $stocks[$i] = "0.00";
               
                $Taxliabilites[$i] = "0.00";
                $totalliabilites[$i] = "0.00";
              
                $totaldebts[$i] = "0.00";
            }
            //end
            $bfr = new BFR();
            $bfr->setBusinessplan($businessSession);
            $bfr->setStocks($stocks);
            $bfr->setTaxliabilites($Taxliabilites);         
            $bfr->setTotalliabilites($totalliabilites);
            $bfr->setTotaldebts($totaldebts);
            $entityManager->merge($bfr);
            
            $exsist = false ;
            $entityManager->flush();
        }
        if($exsist == true){   
            $stock = $bfr[0]->getStocks();
            $taxliabilities = $bfr[0]->getTaxliabilites();
            $totalliabilites = $bfr[0]->getTotalliabilites();
            $totaldebt = $bfr[0]->getTotaldebts();    
            for($i=$rangeofdetail;$i<count($totalliabilites) + $rangeofdetail;$i++){
                $variationtotalliabilitesglobal[$i] +=  $stock[$i - $rangeofdetail] + $totalliabilites[$i - $rangeofdetail]  - $totaldebt[$i - $rangeofdetail];  
                $variationtotalliabilitesglobal[$i+1] -=  $stock[$i - $rangeofdetail]  + $totalliabilites[$i - $rangeofdetail]  - $totaldebt[$i - $rangeofdetail] ;  }
        $form = $this->createForm(BFRFormType::class , $bfr[0]);}
        else{
        $form = $this->createForm(BFRFormType::class , $bfr);}
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $bfr = $form->getData();
            if($exsist == false){
              $entityManager->merge($bfr);
            }
            for($i=$rangeofdetail;$i<count($totalliabilites) + $rangeofdetail;$i++){
                $variationtotalliabilitesglobal[$i] =  "0.00";  
                $variationtotalliabilitesglobal[$i+1] =  "0.00" ;  }
            $stock = $bfr->getStocks();  
            $taxliabilities = $bfr->getTaxliabilites();
            $totalliabilites = $bfr->getTotalliabilites();
            $totaldebt = $bfr->getTotaldebts(); 
            for($i=$rangeofdetail;$i<count($totalliabilites) + $rangeofdetail;$i++){
                $variationtotalliabilitesglobal[$i] +=  $stock[$i - $rangeofdetail] + $totalliabilites[$i - $rangeofdetail]  - $totaldebt[$i - $rangeofdetail];  
                $variationtotalliabilitesglobal[$i+1] -=  $stock[$i - $rangeofdetail]  + $totalliabilites[$i - $rangeofdetail]  - $totaldebt[$i - $rangeofdetail] ;  }
           
            //dump($variationtotalliabilitesglobal,$rangeofdetail);
            $entityManager->flush();
        }
         //decaler la liste variationtotalliabilitesglobal  pour afficher dans le template 
       
       
        $this->detail($request,$SalesdetailledRepository,$productRepository,$SalesRepository,0);
        self::$variationBFRstatic = $this->VariationBFR ;
        self::$variationglobalBFR = $variationtotalliabilitesglobal ;
        
        return $this->render('bfr/index.html.twig', [
            'business' => $businessSession,'CompteClient' => $this->Compteclientforshow,'tvacredit' => $this->tvacredit, 'creancefiscale' => $this->creanceFiscales
            , 'DettesFiscales' =>  $this->DettesFiscales , 'tvaapayer' =>  $this->tvaadecaisser, 'DettesSociale' => $this->DettesSociale, 'variationcreance'=>$this->variationcreances,
            'finaltvadecaisser'=> $this->finaltvadecaisser,'VariationDettes'=> $this->VariationDettes,'finalDetteFiscale' => $this->finalDettesFiscales,'finaltvacredit'=> $this->finaltvacredit,
            'finalcreanceFiscale' => $this->finalcreanceFiscales  ,'VariationBFR' => $this->VariationBFR , 'totalCA' => $this->totalCA,
            'form' => $form->createView() ,  'rangeofglobal' => $rangeofglobal, 'rangeofdetail' =>$rangeofdetail,
            'stocksglobal' => $stock , 'taxliabilities' => $taxliabilities , 'totalliabilites' => $totalliabilites , 'totaldebt'=>$totaldebt
            ,'VariationBFRGlobal' => $variationtotalliabilitesglobal, 
            ]);
    }
    public function getVariationBFR(){
        return self::$variationBFRstatic ;
    }
    public function getVariationGlobal(){
        return self::$variationglobalBFR ;
    }
    /**
     * @Route("-annee-{id}", name="BFRdetail")
     */
    public function detail(Request $request,SalesdetailledRepository $SalesdetailledRepository,ProductRepository $productRepository,SalesRepository $SalesRepository,$id){
        $businessSession =$this->container->get('session')->get('business');
        $years = $businessSession->getNumberofyears();
        $products=$productRepository->findBybusinessplan($businessSession);
        $rangeofdetail  = $businessSession->getRangeofdetail();
        $Compteclientwithname = [];$FinalCompteclientwithname = [];
        //-----------------------------Initialiser les listes de BFR--------------------------------------------------------//
        foreach($products as $product){
        for($i=0;$i<$years;$i++){
        for($x=0;$x<12;$x++){
        $Compteclientwithname[$product->getName()][$i][$x] = "0.00";
        $FinalCompteclientwithname[$product->getName()][$i][$x] ="0.00";
        }
        }}
        for($i=0;$i<$years;$i++){
            for($x=0;$x<12;$x++){
        $this->creanceFiscales[$i][$x] = "0.00";
        $this->DettesFiscales[$i][$x] = "0.00";
        $this->finalcreanceFiscales[$i][$x] = "0.00";
        $this->finalDettesFiscales[$i][$x] = "0.00";
        $this->variationcreances[$i][$x] = "0.00";
        $this->DettesSociale[$i][$x] = "0.00";
        $this->Compteclientforshow[$i][$x] = "0.00";
        $this->finaltvacredit[$i][$x]="0.00";
        $this->variationFiscales[$i][$x]="0.00";
        $this->finaltvadecaisser[$i][$x]="0.00";
           }
        }   
       //
        for($i=0;$i<$years + 1;$i++){
            for($x=0;$x<12;$x++){
                $this->VariationDettes[$i][$x] = "0.00";     
        }}
        for($i=0;$i<$years+1;$i++){
            $this->VariationBFR[$i][11]="0.00";
        }
        //-----------------------------End---------------------------------------------------------------------------------//

        $response = $this->forward('App\Controller\SalesController::receiptyears', [
            'Request' => $request,
            'SalesdetailledRepository'  => $SalesdetailledRepository,
            'ProductRepository' => $productRepository,
            'id' => $id ,
            'SalesRepository' => $SalesRepository,
        ]); 
        $response = $this->forward('App\Controller\SalesController::sales', [
            'request'  => $request,
            'productRepository' => $productRepository,
            'SalesRepository' => $SalesRepository,
        ]);  
        $response = $this->forward('App\Controller\TVAController::index', [
            'id' => $id,
            'request'  => $request,
            'ProductRepository' => $productRepository,
            'SalesdetailledRepository' => $SalesdetailledRepository,    
            'SalesRepository' => $SalesRepository,
            ]); 
        
        $response = $this->forward('App\Controller\ISController::index', [
            'id' => $id,
            'request'  => $request,
                ]);
        $response = $this->forward('App\Controller\StaffController::detail', [
                            'Request'  => $request,
                            'id' => $id,
                            'ProductRepository' => $productRepository,
                            'SalesRepository' => $SalesRepository,
                                
                        ]);    
        $this->totalwithname = SalesController::getTotalwithname();
        $finalCA = SalesController::getfinalca(); 
        $this->tvacredit = TVAController::getcreditTVA();
        $this->tvaadecaisser =  TVAController::getTvadecaisser();
        $isdufin = ISController::getISdufin();
        $DecaissementAdm = StaffController::getDecaissementchargesAdm();
        $DecaissementPro = StaffController::getDecaissementchargesPro();
        $DecaissementCom = StaffController::getDecaissementchargesCom();
        $DecaissementRec = StaffController::getDecaissementchargesRec();
          
        //--------------------------debut de calcul-----------------------------------------//
        //--------------------------totalCA--------------------------------------------------//
        for($x = 0 ; $x < $years ; $x++){
            for($i = 0 ; $i < 12 ; $i++){
              $SumfinalCAperMouth[$x][$i] =  0 ;}}  
        foreach($finalCA as $key=>$value){
            for($x = 0 ; $x < $years ; $x++){
             for($i = 0 ; $i < 12 ; $i++){
               $SumfinalCAperMouth[$x][$i] +=  $finalCA[$key][$x][$i] ;}}}
          
               for($x = 0 ; $x < $years ; $x++){
                $this->totalCA[$x] = array_sum($SumfinalCAperMouth[$x]);
               }
              
        //--------------------------Fin------------------------------------------------------//
        //--------------------------Creance TVA --------------------------------------------//
        $valeur = $this->tvacredit[0][0];
        $this->finaltvacredit[0][0] = $valeur; 
        for($i=0;$i<$years;$i++){
            for($x=0;$x<12;$x++){
                if($x<11){
            if($this->tvacredit[$i][$x]<$this->tvacredit[$i][$x+1]){
                $this->finaltvacredit[$i][$x+1] -= ($this->tvacredit[$i][$x] - $this->tvacredit[$i][$x+1]);
            }
            else if($this->tvacredit[$i][$x]>$this->tvacredit[$i][$x+1]){
                $this->finaltvacredit[$i][$x+1] += ($this->tvacredit[$i][$x+1] - $this->tvacredit[$i][$x]);
            }

        
        }else{
            if($i<$years - 1){
             
            if($this->tvacredit[$i][11]<$this->tvacredit[$i+1][0]){
                $this->finaltvacredit[$i+1][0] -= ($this->tvacredit[$i][11] - $this->tvacredit[$i+1][0]);
            }
            else if($this->tvacredit[$i][11]>$this->tvacredit[$i+1][0]){
                $this->finaltvacredit[$i+1][0] -= ($this->tvacredit[$i][11] - $this->tvacredit[$i+1][0]);
            }
        }
        }}
        }

        //--------------------------Fin-----------------------------------------------------//
        //--------------------------Compte client-------------------------------------------//
       foreach($products as $product){
           $tva = $product->getVat();
       foreach($finalCA as $name=>$value){
       if($product->getName() == $name ){
       foreach($value as $position=>$month){
        foreach($month as $number=>$chiffre){
        
        $Compteclientwithname[$name][$position][$number] =  $chiffre * $tva / 100 + $chiffre ;
       }}}
       }
       }
       foreach($products as $product){
       for($i=0;$i<$years ; $i++){
        for($x =0 ; $x<12 ; $x++){
        ${'chiffre-'.$product->getName().'-'.$i.'-'.$x} = "0.00";}}}
       //cette boucle permet de declarer les chiffre pour le diminuer dans la liste totalwithname
       foreach($Compteclientwithname as $name=>$value){
        for($i=0;$i<$years ; $i++){
        for($x =0 ; $x<12 ; $x++){
        if($value[$i][$x]> 0){
            ${'chiffre-'.$name.'-'.$i.'-'.$x} += $value[$i][$x];
        }}}}

      
       //dump($Compteclientwithname,$this->totalwithname);die();
      
        foreach($Compteclientwithname as $name=>$value){
            $var = 0 ;
            $pos = 0;
            $x = 0 ;
       foreach($this->totalwithname as $productname=>$secondvalue){
           if($name == $productname){
            for($i=0;$i<$years ; $i++){
        
        $var +=  ${'chiffre-'.$name.'-'.$i.'-'.$x};
            $FinalCompteclientwithname[$name][$i][$x] = $var  - $secondvalue[$i][$x];     
            $this->variationcreances[$i][$x] -= $secondvalue[$i][$x] - $value[$i][$x] ;  
            $var -= $secondvalue[$i][$x];
        $x++ ;//1

         $var +=  ${'chiffre-'.$name.'-'.$i.'-'.$x};
            $FinalCompteclientwithname[$name][$i][$x] = $var  - $secondvalue[$i][$x];
            $this->variationcreances[$i][$x] -=  $secondvalue[$i][$x] - $value[$i][$x] ;
            $var -= $secondvalue[$i][$x];
         $x++ ;//2
         
         $var +=  ${'chiffre-'.$name.'-'.$i.'-'.$x};
            $FinalCompteclientwithname[$name][$i][$x] = $var  - $secondvalue[$i][$x]; 
            $this->variationcreances[$i][$x] -=  $secondvalue[$i][$x] - $value[$i][$x];   
            $var -= $secondvalue[$i][$x];
         $x++ ;//3

         $var +=  ${'chiffre-'.$name.'-'.$i.'-'.$x};
            $FinalCompteclientwithname[$name][$i][$x] =  $var  - $secondvalue[$i][$x];
            $this->variationcreances[$i][$x] -=  $secondvalue[$i][$x] - $value[$i][$x];
            $var -= $secondvalue[$i][$x];
          
         $x++ ;//4

         $var +=  ${'chiffre-'.$name.'-'.$i.'-'.$x};
            $FinalCompteclientwithname[$name][$i][$x] =  $var  - $secondvalue[$i][$x];
            $this->variationcreances[$i][$x] -=  $secondvalue[$i][$x] - $value[$i][$x] ; 
            $var -= $secondvalue[$i][$x];
         $x++ ;//5

         $var +=  ${'chiffre-'.$name.'-'.$i.'-'.$x};
            $FinalCompteclientwithname[$name][$i][$x] =  $var  - $secondvalue[$i][$x];
            $this->variationcreances[$i][$x] -=  $secondvalue[$i][$x] - $value[$i][$x];
            $var -= $secondvalue[$i][$x];
         $x++ ;//6

         $var +=  ${'chiffre-'.$name.'-'.$i.'-'.$x};
            $FinalCompteclientwithname[$name][$i][$x] =  $var  - $secondvalue[$i][$x];
            $this->variationcreances[$i][$x] -=  $secondvalue[$i][$x] - $value[$i][$x];
            $var -= $secondvalue[$i][$x];
         $x++ ;//7

         $var +=  ${'chiffre-'.$name.'-'.$i.'-'.$x};
         $FinalCompteclientwithname[$name][$i][$x] =  $var  - $secondvalue[$i][$x];
         $this->variationcreances[$i][$x] -=  $secondvalue[$i][$x] - $value[$i][$x];
         $var -= $secondvalue[$i][$x];
         $x++ ;//8

      $var +=  ${'chiffre-'.$name.'-'.$i.'-'.$x};
      $FinalCompteclientwithname[$name][$i][$x] =  $var  - $secondvalue[$i][$x];
      $this->variationcreances[$i][$x] -=  $secondvalue[$i][$x] - $value[$i][$x];
      $var -= $secondvalue[$i][$x];
      $x++ ;//9

      $var +=  ${'chiffre-'.$name.'-'.$i.'-'.$x};
      $FinalCompteclientwithname[$name][$i][$x] =  $var  - $secondvalue[$i][$x];
      $this->variationcreances[$i][$x] -=  $secondvalue[$i][$x]- $value[$i][$x];
      $var -= $secondvalue[$i][$x];
      $x++ ;//10

      $var +=  ${'chiffre-'.$name.'-'.$i.'-'.$x};
      $FinalCompteclientwithname[$name][$i][$x] =  $var  - $secondvalue[$i][$x];
      $this->variationcreances[$i][$x] -=  $secondvalue[$i][$x] - $value[$i][$x];
      $var -= $secondvalue[$i][$x];
      $x++ ;//11

      $var +=  ${'chiffre-'.$name.'-'.$i.'-'.$x};
      $FinalCompteclientwithname[$name][$i][$x] =  $var  - $secondvalue[$i][$x];
      $this->variationcreances[$i][$x] -=  $secondvalue[$i][$x] - $value[$i][$x];
      $var -= $secondvalue[$i][$x];
      $x++ ;//12
     
      if($x==12){
          $x = 0;
      }
           
        }
           
       }}}
     
     //somme de tout les produits dans une seule liste pour l'afficher
       foreach($FinalCompteclientwithname as $value){
           for($i=0;$i<$years;$i++){
           for($x=0 ; $x<12 ; $x++){
           $this->Compteclientforshow[$i][$x] += $value[$i][$x];
           }
           }
      
       }
     
        //--------------------------Fin Compte client---------------------------------------//
      

        //----------------------------Creance Fiscale---------------------------------------//
        for($i=0;$i<$years;$i++){
        for($x=0;$x<12;$x++){
            if($isdufin[$i][$x] < 0){
            $this->creanceFiscales[$i][$x] = abs($isdufin[$i][$x]);// metter  en valeur absolue
            }
            else {
            $this->DettesFiscales[$i][$x]  = $isdufin[$i][$x] ;
            }
        }
        }
        //---------------------------variation creance fiscale--------------------//
        //---------------------------TVA a payer----------------------------------//
        $valeur2 = $this->tvaadecaisser[0][0];
        $this->finaltvadecaisser[0][0] = $valeur2; 
        for($i=0;$i<$years;$i++){
            for($x=0;$x<12;$x++){
                if($x<11){
            if($this->tvaadecaisser[$i][$x]<$this->tvaadecaisser[$i][$x+1]){
                $this->finaltvadecaisser[$i][$x+1] -= ($this->finaltvadecaisser[$i][$x] - $this->finaltvadecaisser[$i][$x+1]);
            }
            else if($this->tvaadecaisser[$i][$x]>$this->tvaadecaisser[$i][$x+1]){
                $this->finaltvadecaisser[$i][$x+1] += ($this->finaltvadecaisser[$i][$x+1] - $this->finaltvadecaisser[$i][$x]);
            }

        
        }else{
            if($i<$years - 1){
             
            if($this->tvaadecaisser[$i][11]<$this->tvaadecaisser[$i+1][0]){
                $this->finaltvadecaisser[$i+1][0] -= ($this->finaltvadecaisser[$i][11] - $this->finaltvadecaisser[$i+1][0]);
            }
            else if($this->tvaadecaisser[$i][11]>$this->tvaadecaisser[$i+1][0]){
                $this->finaltvadecaisser[$i+1][0] -= ($this->finaltvadecaisser[$i][11] - $this->finaltvadecaisser[$i+1][0]);
            }
        }
        }}
        }
       //dump($this->finaltvadecaisser);die();
        //---------------------------Fin tva a payer------------------------------//
        //---------------------------creance Fisalcale step 2---------------------//
        $valeur2 = $this->creanceFiscales[0][0];
        $this->finalcreanceFiscales[0][0] = $valeur2; 
        for($i=0;$i<$years;$i++){
            for($x=0;$x<12;$x++){
                if($x<11){
            if($this->creanceFiscales[$i][$x]<$this->creanceFiscales[$i][$x+1]){
                $this->finalcreanceFiscales[$i][$x+1] -= ($this->creanceFiscales[$i][$x] - $this->creanceFiscales[$i][$x+1]);
            }
            else if($this->creanceFiscales[$i][$x]>$this->creanceFiscales[$i][$x+1]){
                $this->finalcreanceFiscales[$i][$x+1] += ($this->creanceFiscales[$i][$x+1] - $this->creanceFiscales[$i][$x]);
            }

        
        }else{
            if($i<$years - 1){
             
            if($this->creanceFiscales[$i][11]<$this->creanceFiscales[$i+1][0]){
                $this->finalcreanceFiscales[$i+1][0] -= ($this->creanceFiscales[$i][11] - $this->creanceFiscales[$i+1][0]);
            }
            else if($this->creanceFiscales[$i][11]>$this->creanceFiscales[$i+1][0]){
                $this->finalcreanceFiscales[$i+1][0] -= ($this->creanceFiscales[$i][11] - $this->creanceFiscales[$i+1][0]);
            }
        }
        }}
        }
        //---------------------------fin creance Fiscale--------------------------//
        //-------------------------- DetteFiscale step 2 -------------------------//
        $valeur3 = $this->DettesFiscales[0][0];
        $this->finalDettesFiscales[0][0] = $valeur3; 
        for($i=0;$i<$years;$i++){
            for($x=0;$x<12;$x++){
                if($x<11){
            if($this->DettesFiscales[$i][$x]<$this->DettesFiscales[$i][$x+1]){
                $this->finalDettesFiscales[$i][$x+1] -= ($this->DettesFiscales[$i][$x] - $this->DettesFiscales[$i][$x+1]);
            }
            else if($this->DettesFiscales[$i][$x]>$this->DettesFiscales[$i][$x+1]){
                $this->finalDettesFiscales[$i][$x+1] += ($this->DettesFiscales[$i][$x+1] - $this->DettesFiscales[$i][$x]);
            }

        
        }else{
            if($i<$years - 1){
             
            if($this->DettesFiscales[$i][11]<$this->DettesFiscales[$i+1][0]){
                $this->finalDettesFiscales[$i+1][0] -= ($this->DettesFiscales[$i][11] - $this->DettesFiscales[$i+1][0]);
            }
            else if($this->DettesFiscales[$i][11]>$this->DettesFiscales[$i+1][0]){
                $this->finalDettesFiscales[$i+1][0] -= ($this->DettesFiscales[$i][11] - $this->DettesFiscales[$i+1][0]);
            }
        }
        }}
        }

        //---------------------------Fin DetteFiscale-----------------------------//
        //----------------------------fin-----------------------------------------//
        //----------------------------Fin---------------------------------------------------//
        //----------------------------Dettes Sociale ---------------------------------------//
        for($i=0;$i<$years;$i++){
          for($x=0;$x<11;$x++){
            
            $this->DettesSociale[$i][$x] += $DecaissementAdm[$i][$x+1];
            $this->VariationDettes[$i][$x+1] -= $DecaissementAdm[$i][$x+1];
            $this->VariationDettes[$i][$x] += $DecaissementAdm[$i][$x+1];
            $this->DettesSociale[$i][$x] += $DecaissementPro[$i][$x+1];
            $this->VariationDettes[$i][$x+1] -= $DecaissementPro[$i][$x+1];
            $this->VariationDettes[$i][$x] += $DecaissementPro[$i][$x+1];
            $this->DettesSociale[$i][$x] += $DecaissementCom[$i][$x+1];
            $this->VariationDettes[$i][$x+1] -= $DecaissementCom[$i][$x+1];
            $this->VariationDettes[$i][$x] += $DecaissementCom[$i][$x+1];
            $this->DettesSociale[$i][$x] += $DecaissementRec[$i][$x+1];
            $this->VariationDettes[$i][$x+1] -= $DecaissementRec[$i][$x+1];
            $this->VariationDettes[$i][$x] += $DecaissementRec[$i][$x+1];
            
        if( $x == 10){
            $this->DettesSociale[$i][11] += $DecaissementAdm[$i+1][0];
            $this->VariationDettes[$i+1][0] -= $this->DettesSociale[$i][11];
            $this->DettesSociale[$i][11] += $DecaissementPro[$i+1][0];
            $this->VariationDettes[$i][$x+1] -= $DecaissementPro[$i][$x+1];
            $this->DettesSociale[$i][11] += $DecaissementCom[$i+1][0];
            $this->VariationDettes[$i][$x+1] -= $DecaissementCom[$i][$x+1];
            $this->DettesSociale[$i][11] += $DecaissementRec[$i+1][0];
            $this->VariationDettes[$i][$x+1] -= $DecaissementRec[$i][$x+1];
            $this->VariationDettes[$i][11] += $this->DettesSociale[$i][11];
        }

        }
        }
      
        //--------------------------------Variation BFR------------------------------------//
        for($i=0;$i<$rangeofdetail;$i++){
        $this->VariationBFR[$i][11] +=  ($this->variationcreances[$i][11]  +  $this->tvacredit[$i][11] + $this->creanceFiscales[$i][11]) - ($this->DettesFiscales[$i][11]+$this->tvaadecaisser[$i][11]+$this->DettesSociale[$i][11]);
       
        if($this->VariationBFR[$i][11] >= 0){
        $this->VariationBFR[$i+1][11] += (-1 * (($this->variationcreances[$i][11]  +  $this->tvacredit[$i][11] + $this->creanceFiscales[$i][11]) - ($this->DettesFiscales[$i][11]+$this->tvaadecaisser[$i][11]+$this->DettesSociale[$i][11])));
      
        }
        
        if($this->VariationBFR[$i][11] < 0){
            $this->VariationBFR[$i+1][11] += abs($this->VariationBFR[$i][11]);
        }
       
    }
 
        
        //--------------------------------Fin Variation-------------------------------------//
        //----------------------------Fin---------------------------------------------------//
        
        //-------------------------End------------------------------------------------------//
      
        
        return $this->render('bfr/detail.html.twig', [
            'business' => $businessSession,'CompteClient' => $this->Compteclientforshow[$id] ,'tvacredit' => $this->tvacredit[$id],'finaltvacredit'=> $this->finaltvacredit[$id], 'creancefiscale' => $this->creanceFiscales[$id]
         , 'variationcreance'=>$this->variationcreances[$id]  , 'DettesFiscales' =>  $this->DettesFiscales[$id] , 'tvaapayer' =>  $this->tvaadecaisser[$id], 'DettesSociale' => $this->DettesSociale[$id]
            ,'VariationDettes'=> $this->VariationDettes[$id], 'finaltvadecaisser'=> $this->finaltvadecaisser[$id] , 'finalcreanceFiscale' => $this->finalcreanceFiscales[$id] , 'finalDetteFiscale' => $this->finalDettesFiscales[$id],
            ]);
    }
}
