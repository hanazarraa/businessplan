<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\CompteResultat;
use App\Entity\Loans;
use App\Entity\Planfinancement;
use App\Entity\Tresorerie;
use App\Repository\LoansRepository;
use App\Repository\SalesRepository;
use App\Repository\SalesdetailledRepository;
use App\Repository\ProductRepository;
use App\Form\PlanfinancialFormType;
use App\Repository\GeneralexpensesRepository;
/**
* @Route("/{_locale}/dashboard/my-business-plan/planfinancement")
 */
class PlanfinancementController extends AbstractController
{
    /**
     * @Route("/", name="planfinancement")
     */
    public function index(Request $request,ProductRepository $productRepository ,SalesRepository $SalesRepository,SalesdetailledRepository $SalesdetailledRepository,LoansRepository $loansrepository,GeneralexpensesRepository $generalrep)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $businessSession =$this->container->get('session')->get('business');
        $years = $businessSession->getNumberofyears();
        $rangeofdetail = $businessSession->getRangeofdetail();
        $rangeofglobal = $years  - $rangeofdetail ;
        $RD = $entityManager->getRepository(CompteResultat::class)->findByBusinessplan($businessSession);
        $Loans = $entityManager->getRepository(Loans::class)->findByBusinessplan($businessSession);
        $Plandefinancement = $entityManager->getRepository(Planfinancement::class)->findByBusinessplan($businessSession);
        $Tresorerie = $entityManager->getRepository(Tresorerie::class)->findByBusinessplan($businessSession);
        $exsist = True ;
        //----------------------------Total investissement ------------------------------------------//
        $response = $this->forward('App\Controller\InvestmentsController::index', [
            'Request'  => $request,
        ]);
        $InvestAdm = InvestmentsController::getInvestAdm();
        $InvestPro = InvestmentsController::getInvestPro();
        $InvestCom = InvestmentsController::getInvestCom();
        $InvestRec = InvestmentsController::getInvestRec();
        
        $InvestAdmglobal = InvestmentsController::getInvestAdmglobal();
        $InvestProglobal = InvestmentsController::getInvestProglobal();
        $InvestComglobal = InvestmentsController::getInvestComglobal();
        $InvestRecglobal = InvestmentsController::getInvestRecglobal(); 
        
        
        for($i=0;$i<$rangeofdetail;$i++){
         $totalInvest[$i] = $InvestAdm[$i] + $InvestPro[$i] + $InvestCom[$i] + $InvestRec[$i] ;
        }
     
        for($i=$rangeofdetail;$i<$years;$i++){
          $totalInvest[$i] = $InvestAdmglobal[$i - $rangeofdetail] + $InvestProglobal[$i - $rangeofdetail] + $InvestComglobal[$i - $rangeofdetail] + $InvestRecglobal[$i - $rangeofdetail] ;
        }
        //----------------------------End------------------------------------------//
        //-----------------------------R&D----------------------------------------//
        if($RD == null){
            for($i = 0 ; $i<$years ; $i++){
                $RDlist[$i] = '0.00';
            }
        }
        else {
            $RDlist = $RD[0]->getRD();
        }
       
        //-----------------------------End-----------------------------------------// 
        //-----------------------------Variation BFR-------------------------------//
        $response = $this->forward('App\Controller\BFRController::index', [
            'Request'  => $request,
            'SalesdetailledRepository' => $SalesdetailledRepository,
            'ProductRepository' => $productRepository ,
            'SalesRepository' => $SalesRepository
        ]);
        $variationBFR = BFRController::getVariationBFR(); 
        $variationBFRglobal = BFRController::getVariationGlobal(); 
        
        //-----------------------------End----------------------------------------//
        //----------------------------Loans---------------------------------------//
        $response = $this->forward('App\Controller\LoansController::index', [
            'LoansRepository' => $loansrepository,
            'Request'  => $request,
        ]);
        $capitalRembousent = LoansController::getcapitalRem();
        $emprunts = LoansController::getCapitalemprunt();
       
        //----------------------------End-----------------------------------------//
        //----------------------------Autofinancement-----------------------------//
        $response = $this->forward('App\Controller\CompteresultatController::index', [
            'Request'  => $request,
            'ProductRepository' => $productRepository ,
            'SalesRepository' => $SalesRepository,
            'SalesdetailledRepository' => $SalesdetailledRepository,
            'GeneralexpensesRepository' => $generalrep
        ]);
        $autofinancement = CompteresultatController::getAutofinancement();
      
        //----------------------------End------------------------------------------//
        //---------------------------autre Remboursement-------------------------//
        for($x=0;$x < $years ; $x++){
        $autre[$x] = '0.00'; 
        }
        for($i=0;$i<$rangeofglobal;$i++){
        $totalcapital[$i]  = '0.00';
        $totalcompte[$i]  = '0.00';
        $totalsubventions[$i]  = '0.00';
        $totalavance[$i]  = '0.00';
        }
        if($Plandefinancement ==[]){
            $exsist =  false ; 
            $Plandefinancement = new Planfinancement();
            $Plandefinancement->setAutre($autre);
            $Plandefinancement->setTotalcapital($totalcapital);
            $Plandefinancement->setTotalcompte($totalcompte);
            $Plandefinancement->setTotalsubventions($totalsubventions);
            $Plandefinancement->setTotalavance($totalavance);
            $Plandefinancement->setBusinessplan($businessSession);
            $entityManager->merge($Plandefinancement);
            $entityManager->flush();
        }
        if($exsist == true){
        $form = $this->createForm(PlanfinancialFormType::class , $Plandefinancement[0]);}
        else{
         $form = $this->createForm(PlanfinancialFormType::class , $Plandefinancement);
        }
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $Plandefinancement = $form->getData();
            if($exsist == false){
              $entityManager->merge($Plandefinancement);
            }
            $entityManager->flush();
        }
        //---------------------------End------------------------------------------//  
        //--------------------------get Inputs-----------------------------------//
        $Plandefinancement = $entityManager->getRepository(Planfinancement::class)->findByBusinessplan($businessSession);
        if($Plandefinancement != []){
        $autre = $Plandefinancement[0]->getAutre();
        $totalcapital = $Plandefinancement[0]->getTotalcapital();
        $totalcompte = $Plandefinancement[0]->getTotalcompte();
        $totalsubvention = $Plandefinancement[0]->getTotalsubventions();
        $totaltotalavance = $Plandefinancement[0]->getTotalavance();}
        //--------------------------End------------------------------------------//
        //--------------------------Tresorie detail-----------------------------//
        foreach($Tresorerie as $key=>$value){
        $capitaldetailed[$key] = array_sum($value->getCapital());
        $comptecourantdetailed[$key] = array_sum($value->getCompteCourant());
        $subventionsdetailled[$key] = array_sum($value->getSubventions());
        $avancedetailled[$key] = array_sum($value->getAvances());

        }
        
        //--------------------------End-----------------------------------------//
        return $this->render('planfinancement/index.html.twig', [
            'business' => $businessSession, 'totalInvest' =>  $totalInvest , 'RDList' => $RDlist, 'variationBFR' => $variationBFR 
            ,'variationBFRglobal' =>  $variationBFRglobal , 'capitalremboursement' => $capitalRembousent ,'form' => $form->createView()
            ,'autre' => $autre , 'totalcapital' => $totalcapital , 'totalcompte' => $totalcompte  , 'totalsubvention' => $totalsubvention ,
            'totaltotalavance' =>  $totaltotalavance , 'emprunt' => $emprunts ,
             'capitaldetailed' => $capitaldetailed , "comptecourantdetailed" => $comptecourantdetailed , "subventionsdetailled" => $subventionsdetailled,
            'avancedetailled' => $avancedetailled ,'autofinancement' => $autofinancement,
        ]);
    }
}
